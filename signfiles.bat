@ECHO OFF
ECHO.
ECHO Signing exe files
ECHO.
cd "C:\MacPac\certificate\SHA2\"

signtool sign /d "The Sackett Group" /ac "TSG.cer" /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\daAdmin.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\daAdmin.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDesktopSyncMonitor.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDesktopSyncMonitor.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDesktopSyncProcess.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDesktopSyncProcess.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDesktopSyncMonitor.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDesktopSyncMonitor.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDiag.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fDiag.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteAdministrator.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteAdministrator.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\FortePeopleManager.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\FortePeopleManager.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\FortePeopleManager.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\FortePeopleManager.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\FortePeopleManager.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\FortePeopleManager.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteReg.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteReg.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fPUP.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\fPUP.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\ForteBundleReg.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\ForteBundleReg.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteDBMetadata.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteDBMetadata.exe"

signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteUserContentManager.exe"
signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Forte\Bin\ForteUserContentManager.exe"

rem signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\CI\Tools\Diagnostic\CIClient.exe"
rem signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\CI\Tools\Diagnostic\CIClient.exe"

rem signtool sign /d "The Sackett Group" /ac "TSG.cer"  /fd SHA256 /f "Sackett.pfx" /p "Sackett" /v "C:\Program Files (x86)\The Sackett Group\Numbering\bin\tsgNumReg.exe"
rem signtool timestamp /t http://timestamp.verisign.com/scripts/timstamp.dll "C:\Program Files (x86)\The Sackett Group\Numbering\bin\tsgNumReg.exe"

ECHO.
ECHO Signing complete
ECHO.

PAUSE

CLS
EXIT 